var ctx = document.getElementById("myChart").getContext('2d');
var myChart;
var labels = [];
var datasets = [];
var charData = [];
var chartBgColor;
var chartBorderColor;


function  chartsStarter(station, measurement) {
    var dateSpliter = station.values[0].date.split(" ");
    var secondDateSpliter = station.values[45].date.split(" ");

    document.getElementById('rightDate').innerHTML =  "<span >"+ dateSpliter[0] +"</span> <span style='font-weight: bold'>"+ dateSpliter[1]+"</span>";
    document.getElementById('leftDate').innerHTML =  "<span  style='font-weight: bold'>"+ secondDateSpliter[1] +"</span> <span >"+ secondDateSpliter[0]+"</span>";

    if(myChart!=null) {
        myChart.destroy();
    }
    labels = [];
    datasets = [];


    for(var i = station.values.length-1; i>=0; --i){
        labels.push(station.values[i].date);
        if(measurement == 'PM10') {
            datasets.push(station.values[i].PM10);
            chartBgColor = 'rgba(0, 255, 173, 0.4)';
            chartBorderColor =  'rgba(0, 255, 173,1)';
        }
        if(measurement == 'PM25') {
            datasets.push(station.values[i].PM25);
            chartBgColor = 'rgba(181, 25, 255, 0.4)';
            chartBorderColor =  'rgba(181, 25, 255,1)';
        }
        if(measurement == 'NO2') {
            datasets.push(station.values[i].NO2);
            chartBgColor = 'rgba(0, 30, 255, 0.4)';
            chartBorderColor =  'rgba(0, 30, 255,1)';
        }
        if(measurement == 'SO2') {
            datasets.push(station.values[i].SO2);
            chartBgColor = 'rgba(116, 0, 255, 0.4)';
            chartBorderColor =  'rgba(116, 0, 255,1)';
        }
    }




     var reverse = datasets.slice().reverse();

     myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: measurement + ' μg/m3',
                data: datasets,
                backgroundColor:
                    chartBgColor,
                borderColor:
                    chartBorderColor,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                xAxes: [{

                        display: false

                }]
            }
        }
    });


}