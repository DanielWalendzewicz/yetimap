
function pm10Color( pm10Value){
    if(pm10Value > 0 && pm10Value <= 20){
        $('#pm10-button').css("background-color", "#57B108");
    }
    else if(pm10Value > 20 && pm10Value <= 60){
        $('#pm10-button').css("background-color", "#B0DD10");
    }
    else if(pm10Value > 60 && pm10Value <= 100){
        $('#pm10-button').css("background-color", "#FFD911");
    }
    else if(pm10Value > 100 && pm10Value <= 140){
        $('#pm10-button').css("background-color", "#E58100");
    }
    else if(pm10Value > 140 && pm10Value <= 200){
        $('#pm10-button').css("background-color", "#E50000");
    }
    else if(pm10Value > 200){
        $('#pm10-button').css("background-color", "#990000");
    }
    else if(pm10Value == 0){
        $('#pm10-button').css("background-color", "#4c3899");
    }
};

function pm25Color( pm25Value){
    if(pm25Value > 0 && pm25Value <= 12){
        $('#pm25-button').css("background-color", "#57B108");
    }
    else if(pm25Value > 12 && pm25Value <= 36){
        $('#pm25-button').css("background-color", "#B0DD10");
    }
    else if(pm25Value > 36 && pm25Value <= 60){
        $('#pm25-button').css("background-color", "#FFD911");
    }
    else if(pm25Value > 60 && pm25Value <= 84){
        $('#pm25-button').css("background-color", "#E58100");
    }
    else if(pm25Value > 84 && pm25Value <= 120){
        $('#pm25-button').css("background-color", "#E50000");
    }
    else if(pm25Value > 120){
        $('#pm25-button').css("background-color", "#990000");
    }
    else if(pm25Value == 0){
        $('#pm25-button').css("background-color", "#4c3899");
    }
};

function no2Color( no2Value){
    if(no2Value > 0 && no2Value <= 40){
        $('#no2-button').css("background-color", "#57B108");
    }
    else if(no2Value > 40 && no2Value <= 100){
        $('#no2-button').css("background-color", "#B0DD10");
    }
    else if(no2Value > 100 && no2Value <= 150){
        $('#no2-button').css("background-color", "#FFD911");
    }
    else if(no2Value > 150 && no2Value <= 200){
        $('#no2-button').css("background-color", "#E58100");
    }
    else if(no2Value > 200 && no2Value <= 400){
        $('#no2-button').css("background-color", "#E50000");
    }
    else if(no2Value > 400){
        $('#no2-button').css("background-color", "#990000");
    }
    else if(no2Value == 0){
        $('#no2-button').css("background-color", "#4c3899");
    }
};

function so2Color( so2Value){
    if(so2Value > 0 && so2Value <= 50){
        $('#so2-button').css("background-color", "#57B108");
    }
    else if(so2Value > 50 && so2Value <= 100){
        $('#so2-button').css("background-color", "#B0DD10");
    }
    else if(so2Value > 100 && so2Value <= 200){
        $('#so2-button').css("background-color", "#FFD911");
    }
    else if(so2Value > 200 && so2Value <= 350){
        $('#so2-button').css("background-color", "#E58100");
    }
    else if(so2Value > 351 && so2Value <= 500){
        $('#so2-button').css("background-color", "#E50000");
    }
    else if(so2Value > 500){
        $('#so2-button').css("background-color", "#990000");
    }
    else if(so2Value == 0){
        $('#so2-button').css("background-color", "#4c3899");
    }
};

function NowCastColor( aqiValue) {
    if(aqiValue == 1){
        $('#NowCastColor').css("background-color", "#57B108");
    }
    else if(aqiValue == 2){
        $('#NowCastColor').css("background-color", "#B0DD10");
    }
    else if(aqiValue == 3){
        $('#NowCastColor').css("background-color", "#FFD911");
    }
    else if(aqiValue == 4){
        $('#NowCastColor').css("background-color", "#E58100");
    }
    else if(aqiValue == 5){
        $('#NowCastColor').css("background-color", "#E50000");
    }
    else if(aqiValue == 6){
        $('#NowCastColor').css("background-color", "#990000");
    }
    else if(aqiValue == 0 || aqiValue == null){
        $('#NowCastColor').css("background-color", "#4c3899");
    }
}


function pm10MapPopupColor( pm10Value){
    if(pm10Value > 0 && pm10Value <= 20){
        return "#57B108";
    }
    else if(pm10Value > 20 && pm10Value <= 60){
        return "#B0DD10";
    }
    else if(pm10Value > 60 && pm10Value <= 100){
        return "#FFD911";
    }
    else if(pm10Value > 100 && pm10Value <= 140){
        return "#E58100";
    }
    else if(pm10Value > 140 && pm10Value <= 200){
        return "#E50000";
    }
    else if(pm10Value > 200){
        return  "#990000";
    }
    else if(pm10Value == 0 || pm10Value == null){
        return "#4c3899"
    }
};

function pm25MapPopupColor( pm25Value){
    if(pm25Value > 0 && pm25Value <= 12){
        return "#57B108";
    }
    else if(pm25Value > 12 && pm25Value <= 36){
        return "#B0DD10";
    }
    else if(pm25Value > 36 && pm25Value <= 60){
        return "#FFD911";
    }
    else if(pm25Value > 60 && pm25Value <= 84){
        return "#E58100";
    }
    else if(pm25Value > 84 && pm25Value <= 120){
        return "#E50000";
    }
    else if(pm25Value > 120){
        return "#990000";
    }
    else if(pm25Value == 0 || pm25Value == null ){
        return "#4c3899";
    }
};