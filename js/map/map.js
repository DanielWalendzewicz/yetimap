var map;
var myCircle;
var activeMarkers = [];
var markers = [];


function initializeMapDataFromGios() {

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "https://8hjuayh45i.execute-api.eu-central-1.amazonaws.com/prod/gios-data?province=*",
        success: function (data) {
            $.each(data, function (index, station) {
                stationList.push(station);
            })
            present_date = stationList[0].values[0].date;
            setPmValuesOnMap(present_date);
        }
    });
}

function myMap() {
    var legend = document.getElementById('my-legend');
    var mapProp= {
        center:new google.maps.LatLng(52.281145, 19.456151),
        zoom:7,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map =new google.maps.Map(document.getElementById("map"),mapProp);
    map.setOptions({styles: greeyMap});
    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(legend);
    map.addListener('tilesloaded', function() {
        window.setTimeout(function() {
          $('#my-legend').show();
        }, false);
    });
}
function hideLegend() {
    $('#my-legend').hide();
}
hideLegend();


function zoomProvince(lat, long) {
    var myLatlng = {lat: lat, lng: long};
    map.setZoom(11);
    map.setCenter(myLatlng);
    map.event.trigger(myLatlng, 'click');
}




var defaultBounds = new google.maps.LatLngBounds(
  new google.maps.LatLng(54.39, 18.7)
);

var options = {
  bounds: defaultBounds
};
var inputAutoComplete = document.getElementById('placesAutocomplete');
var autocomplete = new google.maps.places.Autocomplete(inputAutoComplete, options);


google.maps.event.addListener(autocomplete, 'place_changed', function () {
    var place = autocomplete.getPlace();
    if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
    } else {
        map.setCenter(place.geometry.location);
        map.setZoom(15);
    }
});



function mapOfCanvasInitialize( stationName, aqi, pm10, pm25, no2, so2) {
    $('#myChart').show();
    $('#canvas-iframe').hide();
    $('#chartInfo').show();
    $('#offcanvas').trigger("click");
    document.getElementById('gios').innerHTML = 'Stacja GIOŚ';
    document.getElementById('stationName').innerHTML = stationName;
    AqiColor(aqi);


    if(pm25 == 0){
        document.getElementById('pm25-button').innerHTML = "-:-";
    }else {
        document.getElementById('pm25-button').innerHTML = pm25;
    }
    pm25Color(pm25);

    if(pm10 == 0){
        document.getElementById('pm10-button').innerHTML = "-:-";
    }else {
        document.getElementById('pm10-button').innerHTML = pm10;
    }
    pm10Color(pm10);

    if(no2 == 0){
        document.getElementById('no2-button').innerHTML = "-:-";
    }else {
        document.getElementById('no2-button').innerHTML = no2;
    }
    no2Color(no2);

    if(so2 == 0){
        document.getElementById('so2-button').innerHTML = "-:-";
    }else {
        document.getElementById('so2-button').innerHTML = so2;
    }
    so2Color(so2);

}

function AqiColor( aqiValue) {
    if(aqiValue == 1 || aqiValue == 0 || aqiValue == -1){
        $('#aqiColor').css("background-color", "#57B108");
        document.getElementById('aqiColor').innerHTML = "Bardzo dobry";
    }
    else if(aqiValue == 2){
        $('#aqiColor').css("background-color", "#B0DD10");
        document.getElementById('aqiColor').innerHTML = "Dobry";
    }
    else if(aqiValue == 3){
        $('#aqiColor').css("background-color", "#FFD911");
        document.getElementById('aqiColor').innerHTML = "Umiarkowany";
    }
    else if(aqiValue == 4){
        $('#aqiColor').css("background-color", "#E58100");
        document.getElementById('aqiColor').innerHTML = "Dostateczny";
    }
    else if(aqiValue == 5){
        $('#aqiColor').css("background-color", "#E50000");
        document.getElementById('aqiColor').innerHTML = "Zly";
    }
    else if(aqiValue == 6){
        $('#aqiColor').css("background-color", "#990000");
        document.getElementById('aqiColor').innerHTML = "Bardzo zly";
    }
    else if(aqiValue == 0 || aqiValue == null){
        $('#aqiColor').css("background-color", "#4c3899");
        document.getElementById('aqiColor').innerHTML = "-:-";
    }
}

function PM10AqiColor( pm10Value) {
    if(pm10Value > 0 && pm10Value <= 20){
        $('#aqiColor').css("background-color", "#57B108");
        document.getElementById('aqiColor').innerHTML = "Bardzo dobry";
    }
    else if(pm10Value > 20 && pm10Value <= 60){
        $('#aqiColor').css("background-color", "#B0DD10");
        document.getElementById('aqiColor').innerHTML = "Dobry";
    }
    else if(pm10Value > 60 && pm10Value <= 100){
        $('#aqiColor').css("background-color", "#FFD911");
        document.getElementById('aqiColor').innerHTML = "Umiarkowany";
    }
    else if(pm10Value > 100 && pm10Value <= 140){
        $('#aqiColor').css("background-color", "#E58100");
        document.getElementById('aqiColor').innerHTML = "Dostateczny";
    }
    else if(pm10Value > 140 && pm10Value <= 200){
        $('#aqiColor').css("background-color", "#E50000");
        document.getElementById('aqiColor').innerHTML = "Zly";
    }
    else if(pm10Value > 200){
        $('#aqiColor').css("background-color", "#990000");
        document.getElementById('aqiColor').innerHTML = "Bardzo zly";
    }
    else if(pm10Value == 0){
        $('#aqiColor').css("background-color", "#4c3899");
        document.getElementById('aqiColor').innerHTML = "-:-";
    }
}

function setPmValuesOnMap(date) {

    myCircle = new google.maps.Circle({});
    stationList.forEach(function (station) {
        var center = new google.maps.LatLng(station.lat, station.long);
        station.values.forEach(function (value) {
            if(value.date == date) {
                if ( value.aqi == 0 || value.aqi == -1) {

                    var contentInfo = '<div><p style="font-weight: bold; text-align: center; margin-bottom: 3px;">'+station.stationName+'</p>' +
                        '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px;">'+value.date+'</p>' +
                        '<p style="text-align: center; font-weight: bold; margin-bottom: 3px;">' +
                        '<span style=" color:'+ pm10MapPopupColor(value.PM10) +';">PM10: '+ value.PM10 +'</span>' +
                        '<br><span style=" color:'+ pm25MapPopupColor(value.PM25) +';">PM2.5: '+value.PM25+'</span></p></div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentInfo
                    });

                    var marker = new google.maps.Marker({
                        position: center,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            fillOpacity: 1,
                            fillColor: '#57B108',
                            strokeColor: '#ffffff',
                            strokeWeight: 4.0,
                            scale: 11
                        }
                    });

                    marker.setMap(map);

                    marker.addListener('mouseover', function () {
                        infowindow.open(map, marker);
                        marker.setAnimation(google.maps.Animation.BOUNCE);

                    });

                    marker.addListener('mouseout', function () {
                        infowindow.close(map, marker);
                        marker.setAnimation(null);
                    });

                    marker.addListener('click', function () {
                        infowindow.close(map, marker);

                        setInterval(function () {
                            $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                        }, 1000);
                        if(value.PM10 > 0) {
                            chartsStarter(station, 'PM10');
                        } else  {
                            chartsStarter(station, 'NO2');
                        }
                        if(value.PM10 <= 0){
                            $('#pm10-button').attr('disabled', 'true');
                        } else  {
                            $('#pm10-button').removeAttr('disabled');
                            $('#pm10-button').click(function () {
                                chartsStarter(station, 'PM10');
                            });
                        }

                        if(value.NO2 <= 0){
                            $('#no2-button').attr('disabled', 'true');
                        } else {
                            $('#no2-button').removeAttr('disabled');
                            $('#no2-button').click(function () {
                                chartsStarter(station, 'NO2');
                            });
                        }

                        if(value.SO2 <= 0){
                            $('#so2-button').attr('disabled', 'true');
                        } else {
                            $('#so2-button').removeAttr('disabled');
                            $('#so2-button').click(function () {
                                chartsStarter(station, 'SO2');
                            });
                        }

                        if(value.PM25 <= 0){
                            $('#pm25-button').attr('disabled', 'true');
                        }else {
                            $('#pm25-button').removeAttr('disabled');
                            $('#pm25-button').click(function () {
                                chartsStarter(station, 'PM25');
                            });
                        }

                        if(value.PM10 <= 0 && value.PM25 <= 0 && value.SO2 <= 0 && value.NO2 <= 0){
                            myChart.destroy();
                            document.getElementById('rightDate').innerHTML = '';
                            document.getElementById('leftDate').innerHTML = '';
                        }

                        mapOfCanvasInitialize(station.stationName, value.aqi, value.PM10, value.PM25, value.NO2, value.SO2);
                        if($('#body').hasClass("off-canvas-active")){
                            marker.setMap(null);
                            var activeMarker = new google.maps.Marker({
                                position: center,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    fillOpacity: 1,
                                    fillColor: '#57B108',
                                    strokeColor: '#ffffff',
                                    strokeWeight: 4.0,
                                    scale: 18
                                }
                            });
                            activeMarkers = [];
                            activeMarkers.push(activeMarker);
                            activeMarkers[0].setMap(map);
                            activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                            $('#stationName').bind('DOMNodeInserted', function() {
                                infowindow.close(map, marker);
                                activeMarkers[0].setMap(null);
                                marker.setMap(map);

                            });
                            $('#body').on('click', function () {
                                if($('#body').hasClass("off-canvas-active") == false){
                                    activeMarkers[0].setMap(null);
                                    marker.setMap(map);
                                }
                            });
                        }
                    });
                }
                else if (value.aqi == 2 || value.aqi == 1) {
                    var contentInfo = '<div><p style="font-weight: bold; text-align: center; margin-bottom: 3px;">'+station.stationName+'</p>' +
                        '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px;">'+value.date+'</p>' +
                        '<p style="text-align: center; font-weight: bold; margin-bottom: 3px;">' +
                        '<span style=" color:'+ pm10MapPopupColor(value.PM10) +';">PM10: '+ value.PM10 +'</span>' +
                        '<br><span style=" color:'+ pm25MapPopupColor(value.PM25) +';">PM2.5: '+value.PM25+'</span></p></div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentInfo
                    });

                    var marker = new google.maps.Marker({
                        position: center,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            fillOpacity: 1,
                            fillColor: '#B0DD10',
                            strokeColor: '#ffffff',
                            strokeWeight: 4.0,
                            scale: 11
                        }
                    });

                    marker.setMap(map);

                    marker.addListener('mouseover', function () {
                        infowindow.open(map, marker);
                        marker.setAnimation(google.maps.Animation.BOUNCE);
                    });

                    marker.addListener('mouseout', function () {
                        infowindow.close(map, marker);
                        marker.setAnimation(null);
                    });

                    marker.addListener('click', function () {
                        infowindow.close(map, marker);
                        setInterval(function () {
                            $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                        }, 1000);
                        if(value.PM10 > 0) {
                            chartsStarter(station, 'PM10');
                        } else  {
                            chartsStarter(station, 'NO2');
                        }
                        if(value.PM10 <= 0){
                            $('#pm10-button').attr('disabled', 'true');
                        } else  {
                            $('#pm10-button').removeAttr('disabled');
                            $('#pm10-button').click(function () {
                                chartsStarter(station, 'PM10');
                            });
                        }

                        if(value.NO2 <= 0){
                            $('#no2-button').attr('disabled', 'true');
                        } else {
                            $('#no2-button').removeAttr('disabled');
                            $('#no2-button').click(function () {
                                chartsStarter(station, 'NO2');
                            });
                        }

                        if(value.SO2 <= 0){
                            $('#so2-button').attr('disabled', 'true');
                        } else {
                            $('#so2-button').removeAttr('disabled');
                            $('#so2-button').click(function () {
                                chartsStarter(station, 'SO2');
                            });
                        }

                        if(value.PM25 <= 0){
                            $('#pm25-button').attr('disabled', 'true');
                        }else {
                            $('#pm25-button').removeAttr('disabled');
                            $('#pm25-button').click(function () {
                                chartsStarter(station, 'PM25');
                            });
                        }

                        if(value.PM10 <= 0 && value.PM25 <= 0 && value.SO2 <= 0 && value.NO2 <= 0){
                            myChart.destroy();
                            document.getElementById('rightDate').innerHTML = '';
                            document.getElementById('leftDate').innerHTML = '';
                        }

                        mapOfCanvasInitialize(station.stationName, value.aqi, value.PM10, value.PM25, value.NO2, value.SO2);
                        if($('#body').hasClass("off-canvas-active")){
                            marker.setMap(null);
                            var activeMarker = new google.maps.Marker({
                                position: center,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    fillOpacity: 1,
                                    fillColor: '#B0DD10',
                                    strokeColor: '#ffffff',
                                    strokeWeight: 4.0,
                                    scale: 18
                                }
                            });
                            activeMarkers = [];
                            activeMarkers.push(activeMarker);
                            activeMarkers[0].setMap(map);
                            activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                            $('#stationName').bind('DOMNodeInserted', function() {
                                activeMarkers[0].setMap(null);
                                marker.setMap(map);

                            });
                            $('#body').on('click', function () {
                                if($('#body').hasClass("off-canvas-active") == false){
                                    activeMarkers[0].setMap(null);
                                    marker.setMap(map);
                                }
                            });
                        }
                    });
                }
                else if (value.aqi == 3) {
                    var contentInfo = '<div><p style="font-weight: bold; text-align: center; margin-bottom: 3px;">'+station.stationName+'</p>' +
                        '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px;">'+value.date+'</p>' +
                        '<p style="text-align: center; font-weight: bold; margin-bottom: 3px;">' +
                        '<span style=" color:'+ pm10MapPopupColor(value.PM10) +';">PM10: '+ value.PM10 +'</span>' +
                        '<br><span style=" color:'+ pm25MapPopupColor(value.PM25) +';">PM2.5: '+value.PM25+'</span></p></div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentInfo
                    });

                    var marker = new google.maps.Marker({
                        position: center,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            fillOpacity: 1,
                            fillColor: '#FFD911',
                            strokeColor: '#ffffff',
                            strokeWeight: 4.0,
                            scale: 11
                        }
                    });

                    marker.setMap(map);

                    marker.addListener('mouseover', function () {
                        infowindow.open(map, marker);
                        marker.setAnimation(google.maps.Animation.BOUNCE);
                    });

                    marker.addListener('mouseout', function () {
                        infowindow.close(map, marker);
                        marker.setAnimation(null);
                    });

                    marker.addListener('click', function () {
                        infowindow.close(map, marker);
                        setInterval(function () {
                            $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                        }, 1000);
                        if(value.PM10 > 0) {
                            chartsStarter(station, 'PM10');
                        } else  {
                            chartsStarter(station, 'NO2');
                        }
                        if(value.PM10 <= 0){
                            $('#pm10-button').attr('disabled', 'true');
                        } else  {
                            $('#pm10-button').removeAttr('disabled');
                            $('#pm10-button').click(function () {
                                chartsStarter(station, 'PM10');
                            });
                        }

                        if(value.NO2 <= 0){
                            $('#no2-button').attr('disabled', 'true');
                        } else {
                            $('#no2-button').removeAttr('disabled');
                            $('#no2-button').click(function () {
                                chartsStarter(station, 'NO2');
                            });
                        }

                        if(value.SO2 <= 0){
                            $('#so2-button').attr('disabled', 'true');
                        } else {
                            $('#so2-button').removeAttr('disabled');
                            $('#so2-button').click(function () {
                                chartsStarter(station, 'SO2');
                            });
                        }

                        if(value.PM25 <= 0){
                            $('#pm25-button').attr('disabled', 'true');
                        }else {
                            $('#pm25-button').removeAttr('disabled');
                            $('#pm25-button').click(function () {
                                chartsStarter(station, 'PM25');
                            });
                        }

                        if(value.PM10 <= 0 && value.PM25 <= 0 && value.SO2 <= 0 && value.NO2 <= 0){
                            myChart.destroy();
                            document.getElementById('rightDate').innerHTML = '';
                            document.getElementById('leftDate').innerHTML = '';
                        }

                        mapOfCanvasInitialize(station.stationName, value.aqi, value.PM10, value.PM25, value.NO2, value.SO2);
                        if($('#body').hasClass("off-canvas-active")){
                            marker.setMap(null);
                            var activeMarker = new google.maps.Marker({
                                position: center,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    fillOpacity: 1,
                                    fillColor: '#FFD911',
                                    strokeColor: '#ffffff',
                                    strokeWeight: 4.0,
                                    scale: 18
                                }
                            });
                            activeMarkers = [];
                            activeMarkers.push(activeMarker);
                            activeMarkers[0].setMap(map);
                            activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                            $('#stationName').bind('DOMNodeInserted', function() {
                                activeMarkers[0].setMap(null);
                                marker.setMap(map);

                            });
                            $('#body').on('click', function () {
                                if($('#body').hasClass("off-canvas-active") == false){
                                    activeMarkers[0].setMap(null);
                                    marker.setMap(map);
                                }
                            });
                        }
                    });
                }
                else if (value.aqi == 4) {
                    var contentInfo = '<div><p style="font-weight: bold; text-align: center; margin-bottom: 3px;">'+station.stationName+'</p>' +
                        '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px;">'+value.date+'</p>' +
                        '<p style="text-align: center; font-weight: bold; margin-bottom: 3px;">' +
                        '<span style=" color:'+ pm10MapPopupColor(value.PM10) +';">PM10: '+ value.PM10 +'</span>' +
                        '<br><span style=" color:'+ pm25MapPopupColor(value.PM25) +';">PM2.5: '+value.PM25+'</span></p></div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentInfo
                    });

                    var marker = new google.maps.Marker({
                        position: center,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            fillOpacity: 1,
                            fillColor: '#E58100',
                            strokeColor: '#ffffff',
                            strokeWeight: 4.0,
                            scale: 11
                        }
                    });

                    marker.setMap(map);

                    marker.addListener('mouseover', function () {
                        infowindow.open(map, marker);
                        marker.setAnimation(google.maps.Animation.BOUNCE);

                    });

                    marker.addListener('mouseout', function () {
                        infowindow.close(map, marker);
                        marker.setAnimation(null);
                    });

                    marker.addListener('click', function () {
                        infowindow.close(map, marker);
                        setInterval(function () {
                            $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                        }, 1000);
                        if(value.PM10 > 0) {
                            chartsStarter(station, 'PM10');
                        } else  {
                            chartsStarter(station, 'NO2');
                        }
                        if(value.PM10 <= 0){
                            $('#pm10-button').attr('disabled', 'true');
                        } else  {
                            $('#pm10-button').removeAttr('disabled');
                            $('#pm10-button').click(function () {
                                chartsStarter(station, 'PM10');
                            });
                        }

                        if(value.NO2 <= 0){
                            $('#no2-button').attr('disabled', 'true');
                        } else {
                            $('#no2-button').removeAttr('disabled');
                            $('#no2-button').click(function () {
                                chartsStarter(station, 'NO2');
                            });
                        }

                        if(value.SO2 <= 0){
                            $('#so2-button').attr('disabled', 'true');
                        } else {
                            $('#so2-button').removeAttr('disabled');
                            $('#so2-button').click(function () {
                                chartsStarter(station, 'SO2');
                            });
                        }

                        if(value.PM25 <= 0){
                            $('#pm25-button').attr('disabled', 'true');
                        }else {
                            $('#pm25-button').removeAttr('disabled');
                            $('#pm25-button').click(function () {
                                chartsStarter(station, 'PM25');
                            });
                        }

                        if(value.PM10 <= 0 && value.PM25 <= 0 && value.SO2 <= 0 && value.NO2 <= 0){
                            myChart.destroy();
                            document.getElementById('rightDate').innerHTML = '';
                            document.getElementById('leftDate').innerHTML = '';
                        }

                        mapOfCanvasInitialize(station.stationName, value.aqi, value.PM10, value.PM25, value.NO2, value.SO2);
                        if($('#body').hasClass("off-canvas-active")){
                            marker.setMap(null);
                            var activeMarker = new google.maps.Marker({
                                position: center,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    fillOpacity: 1,
                                    fillColor: '#E58100',
                                    strokeColor: '#ffffff',
                                    strokeWeight: 4.0,
                                    scale: 18
                                }
                            });
                            activeMarkers = [];
                            activeMarkers.push(activeMarker);
                            activeMarkers[0].setMap(map);
                            activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                            $('#stationName').bind('DOMNodeInserted', function() {
                                activeMarkers[0].setMap(null);
                                marker.setMap(map);

                            });
                            $('#body').on('click', function () {
                                if($('#body').hasClass("off-canvas-active") == false){
                                    activeMarkers[0].setMap(null);
                                    marker.setMap(map);
                                }
                            });
                        }
                    });
                }
                else if (value.aqi == 5) {
                    var contentInfo = '<div><p style="font-weight: bold; text-align: center; margin-bottom: 3px;">'+station.stationName+'</p>' +
                        '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px;">'+value.date+'</p>' +
                        '<p style="text-align: center; font-weight: bold; margin-bottom: 3px;">' +
                        '<span style=" color:'+ pm10MapPopupColor(value.PM10) +';">PM10: '+ value.PM10 +'</span>' +
                        '<br><span style=" color:'+ pm25MapPopupColor(value.PM25) +';">PM2.5: '+value.PM25+'</span></p></div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentInfo
                    });

                    var marker = new google.maps.Marker({
                        position: center,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            fillOpacity: 1,
                            fillColor: '#E50000',
                            strokeColor: '#ffffff',
                            strokeWeight: 4.0,
                            scale: 11
                        }
                    });

                    marker.setMap(map);

                    marker.addListener('mouseover', function () {
                        infowindow.open(map, marker);
                        marker.setAnimation(google.maps.Animation.BOUNCE);
                    });

                    marker.addListener('mouseout', function () {
                        infowindow.close(map, marker);
                        marker.setAnimation(null);
                    });

                    marker.addListener('click', function () {
                        infowindow.close(map, marker);
                        setInterval(function () {
                            $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                        }, 1000);
                        if(value.PM10 > 0) {
                            chartsStarter(station, 'PM10');
                        } else  {
                            chartsStarter(station, 'NO2');
                        }
                        if(value.PM10 <= 0){
                            $('#pm10-button').attr('disabled', 'true');
                        } else  {
                            $('#pm10-button').removeAttr('disabled');
                            $('#pm10-button').click(function () {
                                chartsStarter(station, 'PM10');
                            });
                        }

                        if(value.NO2 <= 0){
                            $('#no2-button').attr('disabled', 'true');
                        } else {
                            $('#no2-button').removeAttr('disabled');
                            $('#no2-button').click(function () {
                                chartsStarter(station, 'NO2');
                            });
                        }

                        if(value.SO2 <= 0){
                            $('#so2-button').attr('disabled', 'true');
                        } else {
                            $('#so2-button').removeAttr('disabled');
                            $('#so2-button').click(function () {
                                chartsStarter(station, 'SO2');
                            });
                        }

                        if(value.PM25 <= 0){
                            $('#pm25-button').attr('disabled', 'true');
                        }else {
                            $('#pm25-button').removeAttr('disabled');
                            $('#pm25-button').click(function () {
                                chartsStarter(station, 'PM25');
                            });
                        }

                        if(value.PM10 <= 0 && value.PM25 <= 0 && value.SO2 <= 0 && value.NO2 <= 0){
                            myChart.destroy();
                            document.getElementById('rightDate').innerHTML = '';
                            document.getElementById('leftDate').innerHTML = '';
                        }

                        mapOfCanvasInitialize(station.stationName, value.aqi, value.PM10, value.PM25, value.NO2, value.SO2);
                        if($('#body').hasClass("off-canvas-active")){
                            marker.setMap(null);
                            var activeMarker = new google.maps.Marker({
                                position: center,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    fillOpacity: 1,
                                    fillColor: '#E50000',
                                    strokeColor: '#ffffff',
                                    strokeWeight: 4.0,
                                    scale: 18
                                }
                            });
                            activeMarkers = [];
                            activeMarkers.push(activeMarker);
                            activeMarkers[0].setMap(map);
                            activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                            $('#stationName').bind('DOMNodeInserted', function() {
                                activeMarkers[0].setMap(null);
                                marker.setMap(map);

                            });
                            $('#body').on('click', function () {
                                if($('#body').hasClass("off-canvas-active") == false){
                                    activeMarkers[0].setMap(null);
                                    marker.setMap(map);
                                }
                            });
                        }
                    });
                }
                else if (value.aqi == 6) {
                    var contentInfo = '<div><p style="font-weight: bold; text-align: center; margin-bottom: 3px;">'+station.stationName+'</p>' +
                        '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px;">'+value.date+'</p>' +
                        '<p style="text-align: center; font-weight: bold; margin-bottom: 3px;">' +
                        '<span style=" color:'+ pm10MapPopupColor(value.PM10) +';">PM10: '+ value.PM10 +'</span>' +
                        '<br><span style=" color:'+ pm25MapPopupColor(value.PM25) +';">PM2.5: '+value.PM25+'</span></p></div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentInfo
                    });

                    var marker = new google.maps.Marker({
                        position: center,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            fillOpacity: 1,
                            fillColor: '#990000',
                            strokeColor: '#ffffff',
                            strokeWeight: 4.0,
                            scale: 11
                        }
                    });

                    marker.setMap(map);

                    marker.addListener('mouseover', function () {
                        infowindow.open(map, marker);
                        marker.setAnimation(google.maps.Animation.BOUNCE);
                    });

                    marker.addListener('mouseout', function () {
                        infowindow.close(map, marker);
                        marker.setAnimation(null);
                    });

                    marker.addListener('click', function () {
                        infowindow.close(map, marker);
                        setInterval(function () {
                            $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                        }, 1000);
                        if(value.PM10 > 0) {
                            chartsStarter(station, 'PM10');
                        } else  {
                            chartsStarter(station, 'NO2');
                        }
                        if(value.PM10 <= 0){
                            $('#pm10-button').attr('disabled', 'true');
                        } else  {
                            $('#pm10-button').removeAttr('disabled');
                            $('#pm10-button').click(function () {
                                chartsStarter(station, 'PM10');
                            });
                        }

                        if(value.NO2 <= 0){
                            $('#no2-button').attr('disabled', 'true');
                        } else {
                            $('#no2-button').removeAttr('disabled');
                            $('#no2-button').click(function () {
                                chartsStarter(station, 'NO2');
                            });
                        }

                        if(value.SO2 <= 0){
                            $('#so2-button').attr('disabled', 'true');
                        } else {
                            $('#so2-button').removeAttr('disabled');
                            $('#so2-button').click(function () {
                                chartsStarter(station, 'SO2');
                            });
                        }

                        if(value.PM25 <= 0){
                            $('#pm25-button').attr('disabled', 'true');
                        }else {
                            $('#pm25-button').removeAttr('disabled');
                            $('#pm25-button').click(function () {
                                chartsStarter(station, 'PM25');
                            });
                        }

                        if(value.PM10 <= 0 && value.PM25 <= 0 && value.SO2 <= 0 && value.NO2 <= 0){
                            myChart.destroy();
                            document.getElementById('rightDate').innerHTML = '';
                            document.getElementById('leftDate').innerHTML = '';
                        }

                        mapOfCanvasInitialize(station.stationName, value.aqi, value.PM10, value.PM25, value.NO2, value.SO2);
                        if($('#body').hasClass("off-canvas-active")){
                            marker.setMap(null);
                            var activeMarker = new google.maps.Marker({
                                position: center,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    fillOpacity: 1,
                                    fillColor: '#990000',
                                    strokeColor: '#ffffff',
                                    strokeWeight: 4.0,
                                    scale: 18
                                }
                            });
                            activeMarkers = [];
                            activeMarkers.push(activeMarker);
                            activeMarkers[0].setMap(map);
                            activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                            $('#stationName').bind('DOMNodeInserted', function() {
                                activeMarkers[0].setMap(null);
                                marker.setMap(map);

                            });
                            $('#body').on('click', function () {
                                if($('#body').hasClass("off-canvas-active") == false){
                                    activeMarkers[0].setMap(null);
                                    marker.setMap(map);
                                }
                            });
                        }
                    });
                }
            }
        });



    })



}

