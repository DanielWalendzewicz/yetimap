

var privateClientsMarkersApi = 'https://i2xmhrwwsh.execute-api.eu-central-1.amazonaws.com/prod/map-data/';
var privateClientsStationList = [];
var myCircle;
var privateStationsOnMap = false;

var PortGdyniaVeryGoodMarkerObw = 'img/seadata/private_map_markers/PortGdyniaVeryGoodMarkerObw.svg';
var PortGdyniaGoodMarkerObw = 'img/seadata/private_map_markers/PortGdyniaGoodMarkerObw.svg';
var PortGdyniaSufficientMarkerObw = 'img/seadata/private_map_markers/PortGdyniaSufficientMarkerObw.svg';
var PortGdyniaModerateMarkerObw = 'img/seadata/private_map_markers/PortGdyniaModerateMarkerObw.svg';
var PortGdyniaBadMarkerObw = 'img/seadata/private_map_markers/PortGdyniaBadMarkerObw.svg';
var PortGdyniaVeryBadMarkerObw = 'img/seadata/private_map_markers/PortGdyniaVeryBadMarkerObw.svg';



var PortGdyniaMarkersListObw = [PortGdyniaVeryGoodMarkerObw, PortGdyniaGoodMarkerObw, PortGdyniaSufficientMarkerObw, PortGdyniaModerateMarkerObw,
PortGdyniaBadMarkerObw, PortGdyniaVeryBadMarkerObw];

function zoomMapOnPrivateClient( privLat, privLong) {
    map.setCenter({lat:privLat, lng:privLong});
    map.setZoom(14);
}

function initializePriveClientMapData( clientName) {

    $.ajax({
        type: "GET",
        dataType: "json",
        url: privateClientsMarkersApi + clientName,
        success: function (data) {
            $.each(data, function (index, station) {
               privateClientsStationList.push(station);
            })
        }
    });
}

// , pm10Dashboard, pm25Dashboard
function privateMapOfCanvasInitialize( stationName, stationLogo, pm10, pm25, dashboard) {
    $('#chartInfo').hide();
    $('#offcanvas').trigger("click");
    document.getElementById('gios').innerHTML = " <img src=" +stationLogo+ "  height='110' width='260'>";
    document.getElementById('stationName').innerHTML = stationName;
    PM10AqiColor(pm10);

    if(pm25 == 0){
        document.getElementById('pm25-button').innerHTML = "-:-";
    }else {
        document.getElementById('pm25-button').innerHTML = pm25;
    }
    pm25Color(pm25);

    if(pm10 == 0){
        document.getElementById('pm10-button').innerHTML = "-:-";
    }else {
        document.getElementById('pm10-button').innerHTML = pm10;
    }
    pm10Color(pm10);

        document.getElementById('no2-button').innerHTML = "-:-";
    no2Color(0);

        document.getElementById('so2-button').innerHTML = "-:-";
    so2Color(0);

    $('#myChart').hide();
    $('#canvas-iframe').show();
    $('#canvas-iframe').removeAttr('hidden');
    $('#canvas-iframe').attr('src', dashboard);


}

function setPrivatePmValuesOnMap(stationPrivateMarkersListObw, privateStationsList) {

    myCircle = new google.maps.Circle({});
    privateStationsList.forEach(function (station) {
        privateStationsOnMap = true;
        if(station.stationName != undefined){
        var center = new google.maps.LatLng(station.lat, station.long);
        if (station.last_value[0].PM10 > 0 && station.last_value[0].PM10 <= 20 || station.last_value[0].PM25 > 0 && station.last_value[0].PM25 <= 12) {

            var contentInfo = '<div><p style="font-weight: bold; text-align: center;  margin-bottom: 3px">'+station.stationName+'</p>' +
                '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px">'+station.last_value[0].date+'</p>' +
                '<p style="text-align: center; font-weight: bold;  margin-left: 5px;  margin-bottom: 3px">' +
                '<span style=" color:'+ pm10MapPopupColor(station.last_value[0].PM10) +';">PM10: '+station.last_value[0].PM10+'</span>' +
                '<br><span style=" color:'+ pm25MapPopupColor(station.last_value[0].PM25) +';">PM2.5: '+station.last_value[0].PM25+'</span></p></div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentInfo
            });

            var icon = {
                url: stationPrivateMarkersListObw[0], // url
                scaledSize: new google.maps.Size(35, 35), // scaled size
            };

            var marker = new google.maps.Marker({
                position: center,
                icon: icon
            });

            marker.setMap(map);

            marker.addListener('mouseover', function () {
                infowindow.open(map, marker);
                marker.setAnimation(google.maps.Animation.BOUNCE);

            });

            marker.addListener('mouseout', function () {
                infowindow.close(map, marker);
                marker.setAnimation(null);
            });

            marker.addListener('click', function () {
                infowindow.close(map, marker);

                setInterval(function () {
                    $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                }, 1000);



                    $('#no2-button').attr('disabled', 'true');
                    $('#so2-button').attr('disabled', 'true');
                $('#pm10-button').attr('disabled', 'true');
                    $('#pm25-button').attr('disabled', 'true');

                    document.getElementById('rightDate').innerHTML = '';
                    document.getElementById('leftDate').innerHTML = '';

                privateMapOfCanvasInitialize(station.stationName, 'img/seadata/PortGdyniaLogoPoziom.jpg', station.last_value[0].PM10, station.last_value[0].PM25, station.dashboard_url);
                if ($('#body').hasClass("off-canvas-active")) {
                    marker.setMap(null);

                    var iconActive = {
                        url: stationPrivateMarkersListObw[0], // url
                        scaledSize: new google.maps.Size(53, 53), // scaled size
                    };

                    var activeMarker = new google.maps.Marker({
                        position: center,
                        icon: iconActive
                    });

                    activeMarkers = [];
                    activeMarkers.push(activeMarker);
                    activeMarkers[0].setMap(map);
                    activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                    setTimeout(function(){
                        activeMarkers[0].setAnimation(null);
                    }, 2200);

                    $('#stationName').bind('DOMNodeInserted', function () {
                        activeMarkers[0].setMap(null);
                        marker.setMap(map);

                    });
                    $('#body').on('click', function () {
                        if ($('#body').hasClass("off-canvas-active") == false) {
                            activeMarkers[0].setMap(null);
                            marker.setMap(map);
                        }
                    });
                }
            });
        }
        else if (station.last_value[0].PM10 > 20 && station.last_value[0].PM10 <= 60 || station.last_value[0].PM25 > 12 && station.last_value[0].PM10 <= 36) {
            var contentInfo = '<div><p style="font-weight: bold; text-align: center;  margin-bottom: 3px">'+station.stationName+'</p>' +
                '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px">'+station.last_value[0].date+'</p>' +
                '<p style="text-align: center; font-weight: bold;  margin-left: 5px;  margin-bottom: 3px">' +
                '<span style=" color:'+ pm10MapPopupColor(station.last_value[0].PM10) +';">PM10: '+station.last_value[0].PM10+'</span>' +
                '<br><span style=" color:'+ pm25MapPopupColor(station.last_value[0].PM25) +';">PM2.5: '+station.last_value[0].PM25+'</span></p></div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentInfo
            });
            var icon = {
                url: stationPrivateMarkersListObw[1], // url
                scaledSize: new google.maps.Size(35, 35), // scaled size
            };

            var marker = new google.maps.Marker({
                position: center,
                icon: icon
            });

            marker.setMap(map);

            marker.addListener('mouseover', function () {
                infowindow.open(map, marker);
                marker.setAnimation(google.maps.Animation.BOUNCE);

            });

            marker.addListener('mouseout', function () {
                infowindow.close(map, marker);
                marker.setAnimation(null);
            });

            marker.addListener('click', function () {
                infowindow.close(map, marker);

                setInterval(function () {
                    $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                }, 1000);

                    $('#pm10-button').attr('disabled', 'true');

                $('#no2-button').attr('disabled', 'true');
                $('#so2-button').attr('disabled', 'true');

                    $('#pm25-button').attr('disabled', 'true');

                document.getElementById('rightDate').innerHTML = '';
                document.getElementById('leftDate').innerHTML = '';

                privateMapOfCanvasInitialize(station.stationName, 'img/seadata/PortGdyniaLogoPoziom.jpg', station.last_value[0].PM10, station.last_value[0].PM25, station.dashboard_url);
                if ($('#body').hasClass("off-canvas-active")) {
                    marker.setMap(null);

                    var iconActive = {
                        url: stationPrivateMarkersListObw[1], // url
                        scaledSize: new google.maps.Size(53, 53), // scaled size
                    };

                    var activeMarker = new google.maps.Marker({
                        position: center,
                        icon: iconActive
                    });

                    activeMarkers = [];
                    activeMarkers.push(activeMarker);
                    activeMarkers[0].setMap(map);
                    activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                    $('#stationName').bind('DOMNodeInserted', function () {
                        activeMarkers[0].setMap(null);
                        marker.setMap(map);

                    });
                    $('#body').on('click', function () {
                        if ($('#body').hasClass("off-canvas-active") == false) {
                            activeMarkers[0].setMap(null);
                            marker.setMap(map);
                        }
                    });
                }
            });
        }
        else if (station.last_value[0].PM10 > 60 && station.last_value[0].PM10 <= 100 || station.last_value[0].PM25 > 36 && station.last_value[0].PM10 <= 60) {
            var contentInfo = '<div><p style="font-weight: bold; text-align: center;  margin-bottom: 3px">'+station.stationName+'</p>' +
                '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px">'+station.last_value[0].date+'</p>' +
                '<p style="text-align: center; font-weight: bold;  margin-left: 5px;  margin-bottom: 3px">' +
                '<span style=" color:'+ pm10MapPopupColor(station.last_value[0].PM10) +';">PM10: '+station.last_value[0].PM10+'</span>' +
                '<br><span style=" color:'+ pm25MapPopupColor(station.last_value[0].PM25) +';">PM2.5: '+station.last_value[0].PM25+'</span></p></div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentInfo
            });
            var icon = {
                url: stationPrivateMarkersListObw[2], // url
                scaledSize: new google.maps.Size(35, 35), // scaled size
            };

            var marker = new google.maps.Marker({
                position: center,
                icon: icon
            });

            marker.setMap(map);

            marker.addListener('mouseover', function () {
                infowindow.open(map, marker);
                marker.setAnimation(google.maps.Animation.BOUNCE);

            });

            marker.addListener('mouseout', function () {
                infowindow.close(map, marker);
                marker.setAnimation(null);
            });

            marker.addListener('click', function () {
                infowindow.close(map, marker);

                setInterval(function () {
                    $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                }, 1000);

                if (station.last_value[0].PM10 <= 0) {
                    $('#pm10-button').attr('disabled', 'true');
                } else {
                    $('#pm10-button').removeAttr('disabled');
                }

                $('#no2-button').attr('disabled', 'true');
                $('#so2-button').attr('disabled', 'true');

                if (station.last_value[0].PM25 <= 0) {
                    $('#pm25-button').attr('disabled', 'true');
                } else {
                    $('#pm25-button').removeAttr('disabled');
                }
                document.getElementById('rightDate').innerHTML = '';
                document.getElementById('leftDate').innerHTML = '';

                privateMapOfCanvasInitialize(station.stationName, 'img/seadata/PortGdyniaLogoPoziom.jpg', station.last_value[0].PM10, station.last_value[0].PM25, station.dashboard_url);
                if ($('#body').hasClass("off-canvas-active")) {
                    marker.setMap(null);

                    var iconActive = {
                        url: stationPrivateMarkersListObw[2], // url
                        scaledSize: new google.maps.Size(53, 53), // scaled size
                    };

                    var activeMarker = new google.maps.Marker({
                        position: center,
                        icon: iconActive
                    });

                    activeMarkers = [];
                    activeMarkers.push(activeMarker);
                    activeMarkers[0].setMap(map);
                    activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                    $('#stationName').bind('DOMNodeInserted', function () {
                        activeMarkers[0].setMap(null);
                        marker.setMap(map);

                    });
                    $('#body').on('click', function () {
                        if ($('#body').hasClass("off-canvas-active") == false) {
                            activeMarkers[0].setMap(null);
                            marker.setMap(map);
                        }
                    });
                }
            });
        }
        else if (station.last_value.PM10 > 100 && station.last_value.PM10 <= 140 || station.last_value.PM25 > 60 && station.last_value.PM10 <= 84) {
            var contentInfo = '<div><p style="font-weight: bold; text-align: center;  margin-bottom: 3px">'+station.stationName+'</p>' +
                '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px">'+station.last_value[0].date+'</p>' +
                '<p style="text-align: center; font-weight: bold;  margin-left: 5px;  margin-bottom: 3px">' +
                '<span style=" color:'+ pm10MapPopupColor(station.last_value[0].PM10) +';">PM10: '+station.last_value[0].PM10+'</span>' +
                '<br><span style=" color:'+ pm25MapPopupColor(station.last_value[0].PM25) +';">PM2.5: '+station.last_value[0].PM25+'</span></p></div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentInfo
            });
            var icon = {
                url: stationPrivateMarkersListObw[3], // url
                scaledSize: new google.maps.Size(35, 35), // scaled size
            };

            var marker = new google.maps.Marker({
                position: center,
                icon: icon
            });

            marker.setMap(map);

            marker.addListener('mouseover', function () {
                infowindow.open(map, marker);
                marker.setAnimation(google.maps.Animation.BOUNCE);

            });

            marker.addListener('mouseout', function () {
                infowindow.close(map, marker);
                marker.setAnimation(null);
            });

            marker.addListener('click', function () {
                infowindow.close(map, marker);

                setInterval(function () {
                    $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                }, 1000);

                if (station.last_value[0].PM10 <= 0) {
                    $('#pm10-button').attr('disabled', 'true');
                } else {
                    $('#pm10-button').removeAttr('disabled');
                }

                $('#no2-button').attr('disabled', 'true');
                $('#so2-button').attr('disabled', 'true');

                if (station.last_value[0].PM25 <= 0) {
                    $('#pm25-button').attr('disabled', 'true');
                } else {
                    $('#pm25-button').removeAttr('disabled');
                }
                document.getElementById('rightDate').innerHTML = '';
                document.getElementById('leftDate').innerHTML = '';

                privateMapOfCanvasInitialize(station.stationName, 'img/seadata/PortGdyniaLogoPoziom.jpg', station.last_value[0].PM10, station.last_value[0].PM25, station.dashboard_url);
                if ($('#body').hasClass("off-canvas-active")) {
                    marker.setMap(null);

                    var iconActive = {
                        url: stationPrivateMarkersListObw[3], // url
                        scaledSize: new google.maps.Size(53, 53), // scaled size
                    };

                    var activeMarker = new google.maps.Marker({
                        position: center,
                        icon: iconActive
                    });

                    activeMarkers = [];
                    activeMarkers.push(activeMarker);
                    activeMarkers[0].setMap(map);
                    activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                    $('#stationName').bind('DOMNodeInserted', function () {
                        activeMarkers[0].setMap(null);
                        marker.setMap(map);

                    });
                    $('#body').on('click', function () {
                        if ($('#body').hasClass("off-canvas-active") == false) {
                            activeMarkers[0].setMap(null);
                            marker.setMap(map);
                        }
                    });
                }
            });
        }
        else if (station.last_value.PM10 > 140 && station.last_value.PM10 <= 200 || station.last_value.PM25 > 84 && station.last_value.PM10 <= 120) {
            var contentInfo = '<div><p style="font-weight: bold; text-align: center;  margin-bottom: 3px">'+station.stationName+'</p>' +
                '<p style="text-align: center; font-weight: bold; color: lightgrey; font-size: x-small; margin-left: 5px; margin-bottom: 3px">'+station.last_value[0].date+'</p>' +
                '<p style="text-align: center; font-weight: bold;  margin-left: 5px;  margin-bottom: 3px">' +
                '<span style=" color:'+ pm10MapPopupColor(station.last_value[0].PM10) +';">PM10: '+station.last_value[0].PM10+'</span>' +
                '<br><span style=" color:'+ pm25MapPopupColor(station.last_value[0].PM25) +';">PM2.5: '+station.last_value[0].PM25+'</span></p></div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentInfo
            });
            var icon = {
                url: stationPrivateMarkersListObw[4], // url
                scaledSize: new google.maps.Size(35, 35), // scaled size
            };

            var marker = new google.maps.Marker({
                position: center,
                icon: icon
            });

            marker.setMap(map);

            marker.addListener('mouseover', function () {
                infowindow.open(map, marker);
                marker.setAnimation(google.maps.Animation.BOUNCE);

            });

            marker.addListener('mouseout', function () {
                infowindow.close(map, marker);
                marker.setAnimation(null);
            });

            marker.addListener('click', function () {
                infowindow.close(map, marker);

                setInterval(function () {
                    $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                }, 1000);

                if (station.last_value[0].PM10 <= 0) {
                    $('#pm10-button').attr('disabled', 'true');
                } else {
                    $('#pm10-button').removeAttr('disabled');
                }

                $('#no2-button').attr('disabled', 'true');
                $('#so2-button').attr('disabled', 'true');

                if (station.last_value[0].PM25 <= 0) {
                    $('#pm25-button').attr('disabled', 'true');
                } else {
                    $('#pm25-button').removeAttr('disabled');
                }
                document.getElementById('rightDate').innerHTML = '';
                document.getElementById('leftDate').innerHTML = '';

                privateMapOfCanvasInitialize(station.stationName, 'img/seadata/PortGdyniaLogoPoziom.jpg', station.last_value[0].PM10, station.last_value[0].PM25, station.dashboard_url);
                if ($('#body').hasClass("off-canvas-active")) {
                    marker.setMap(null);

                    var iconActive = {
                        url: stationPrivateMarkersListObw[4], // url
                        scaledSize: new google.maps.Size(53, 53), // scaled size
                    };

                    var activeMarker = new google.maps.Marker({
                        position: center,
                        icon: iconActive
                    });

                    activeMarkers = [];
                    activeMarkers.push(activeMarker);
                    activeMarkers[0].setMap(map);
                    activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                    $('#stationName').bind('DOMNodeInserted', function () {
                        activeMarkers[0].setMap(null);
                        marker.setMap(map);

                    });
                    $('#body').on('click', function () {
                        if ($('#body').hasClass("off-canvas-active") == false) {
                            activeMarkers[0].setMap(null);
                            marker.setMap(map);
                        }
                    });
                }
            });
        }
        else if (station.last_value.PM10 > 200 || station.last_value.PM25 > 120) {
            var icon = {
                url: stationPrivateMarkersListObw[5], // url
                scaledSize: new google.maps.Size(35, 35), // scaled size
            };

            var marker = new google.maps.Marker({
                position: center,
                icon: icon
            });

            marker.setMap(map);

            marker.addListener('mouseover', function () {
                infowindow.open(map, marker);
                marker.setAnimation(google.maps.Animation.BOUNCE);

            });

            marker.addListener('mouseout', function () {
                infowindow.close(map, marker);
                marker.setAnimation(null);
            });

            marker.addListener('click', function () {
                infowindow.close(map, marker);

                setInterval(function () {
                    $('.dot').fadeOut(1000).fadeIn(1500).fadeOut(1500);
                }, 1000);

                if (station.last_value[0].PM10 <= 0) {
                    $('#pm10-button').attr('disabled', 'true');
                } else {
                    $('#pm10-button').removeAttr('disabled');
                }

                $('#no2-button').attr('disabled', 'true');
                $('#so2-button').attr('disabled', 'true');

                if (station.last_value[0].PM25 <= 0) {
                    $('#pm25-button').attr('disabled', 'true');
                } else {
                    $('#pm25-button').removeAttr('disabled');
                }
                document.getElementById('rightDate').innerHTML = '';
                document.getElementById('leftDate').innerHTML = '';

                privateMapOfCanvasInitialize(station.stationName, 'img/seadata/PortGdyniaLogoPoziom.jpg', station.last_value[0].PM10, station.last_value[0].PM25, station.dashboard_url);
                if ($('#body').hasClass("off-canvas-active")) {
                    marker.setMap(null);

                    var iconActive = {
                        url: stationPrivateMarkersListObw[5], // url
                        scaledSize: new google.maps.Size(53, 53), // scaled size
                    };

                    var activeMarker = new google.maps.Marker({
                        position: center,
                        icon: iconActive
                    });

                    activeMarkers = [];
                    activeMarkers.push(activeMarker);
                    activeMarkers[0].setMap(map);
                    activeMarkers[0].setAnimation(google.maps.Animation.BOUNCE);
                    $('#stationName').bind('DOMNodeInserted', function () {
                        activeMarkers[0].setMap(null);
                        marker.setMap(map);

                    });
                    $('#body').on('click', function () {
                        if ($('#body').hasClass("off-canvas-active") == false) {
                            activeMarkers[0].setMap(null);
                            marker.setMap(map);
                        }
                    });
                }
            });
        }


    }
    })



}

