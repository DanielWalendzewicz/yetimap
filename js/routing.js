

$(function () {
    var ApplicationRouter = Backbone.Router.extend({

        routes: {
            "PortGdynia": "PortGdynia"
        },

        PortGdynia: function () {

           zoomMapOnPrivateClient(54.529991,18.542658);
            $(window).scrollTop($('#about').offset().top);
            if(privateStationsOnMap == false) {
                setPrivatePmValuesOnMap(PortGdyniaMarkersListObw, privateClientsStationList);
            }

        }

    });


    var ApplicationView = Backbone.View.extend({

        el: $('#about'),

        events: {
            'click #PortGdyniaButton': 'displayPortGdynia'
        },

        initialize: function(){
            //set dependency on ApplicationRouter
            this.router = new ApplicationRouter();
        },

        displayPortGdynia: function(e) {
            e.stopPropagation();
            this.router.navigate("PortGdynia", true);

        }


    });

    new ApplicationView();

});